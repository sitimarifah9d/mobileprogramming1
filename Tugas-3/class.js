//No. 1 - Animal Class 
console.log("---- Soal 1 ----")
class Animal {
  constructor(name,legs,cold_blooded) {
    this.name = "Shaun"
    this.legs = 4
    this.cold_blooded = "false"
  }
}
      
   var sheep = new Animal("shaun");
   console.log(sheep.name) // "shaun"
   console.log(sheep.legs) // 4
   console.log(sheep.cold_blooded) // false

console.log('\n')
//No.2 - Function to Class
class Clock {
  
}
var clock = new Clock({template: 'h:m:s'});
clock.start();
