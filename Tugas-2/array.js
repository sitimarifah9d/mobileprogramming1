function range(startNum, finishNum) {
    var x = [];
    if (startNum == null) {
    return (0);
    }
    return x.reduce ((a, b) => a + b, 1);
}
var range = [12, 1, 3]
var descOrder = range.sort((a,b) => a>b?1:-1)
    return (12 - 1 - 3)
console.log(range(1,10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54,50));
console.log(range());


//No.2 (Range with step)
function rangeWithStep(startNum, finishNum, step) {
    var x = [];
    if (startNum == null) {
    return (0);
    }
    if (step > 0) {
    for (var y = startNum; y <= finishNum; y += step) {
    x.push(y)
    }
    } else {
    for (var z = startNum; z >= finishNum; z += step) {
    x.push(z)
    }
    }
    return x.reduce ((a, b) => a + b, 1);
}
console.log(rangeWithStep(1,10,2));
console.log(rangeWithStep(11,23,3));
console.log(rangeWithStep(5,2,1));
console.log(rangeWithStep(29,2,4));


console.log('\n')
//No. 3 (Sum of Range)
function sum(startNum, finishNum, step) {
    var x = [];
    if (startNum == null) {
    return (0);
    }
    if (step > 0) {
    for (var y = startNum; y <= finishNum; y += step) {
    x.push(y)
    }
    } else {
    for (var z = startNum; z >= finishNum; z += step) {
    x.push(z)
    }
    }
    return x.reduce ((a, b) => a + b, 1);
    }
    console.log(sum(1,10));
    console.log(sum(5, 50, 2));
    console.log(sum(15,10));
    console.log(sum(20, 10, -2));
    console.log(sum(1));
    console.log(sum());

    console.log('\n')
    //No.4 (Array Multidimensi)
    function dataHandling(bioArr){
        for(var i = 0; i < bioArr.length; i++){
          console.log("Nomor ID: " + bioArr[i][0]);
          console.log("Nama Lengkap: " + bioArr[i][1]);
          console.log("TTL: " + bioArr[i][2] + " " + bioArr[i][3]);
          console.log("Hobi: " + bioArr[i][4]);
          console.log("");
        }
      }
      
      var input = [
                      ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                      ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                      ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                      ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
                  ]
      
      console.log(dataHandling(input));

console.log('\n')
//No.5 (Balik Kata)
function balikKata(kata) {
    return kata.split("").reverse().join("");
  }
  
  // TEST CASES
  console.log(balikKata('Kasur Rusak')); 
  console.log(balikKata('Informatika'));
  console.log(balikKata('Haji Ijah'));
  console.log(balikKata('rececar'));
  console.log(balikKata('I am Humanikers'));

      
console.log('\n')
//No.6 (Metode Array)
function dataHandling2(bioArr){
  const newBioArr = bioArr;
  newBioArr.splice(1,2,"Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
  newBioArr.splice(4,1, "Pria", "SMA Internasional Metro");
        
  const name = newBioArr[1];
  const month = newBioArr[3].split("/")[1]
  const dateArr = newBioArr[3].split("/")
  dateArr.sort(function(a, b) {
    return b-a
    });
    const dateString = newBioArr[3].split("/").join("-")
        
    switch(parseInt(month)){
      case 01:
        monthName = 'Januari';
        break;
      case 02:
        monthName = 'Febuari';
        break;
      case 03:
        monthName = 'Maret';
        break;
      case 04:
        monthName = 'April';
        break;
      case 05:
        monthName = 'Mei';
        break;
      case 06:
        monthName = 'Juni';
        break;
      case 07:
        monthName = 'Juli';
        break;
      case 08:
        monthName = 'Agustus';
        break;
      case 09:
        monthName = 'September';
        break;
      case 10:
        monthName = 'Oktober';
        break;
      case 11:
        monthName = 'November';
        break;
      case 12:
        monthName = 'Desember';
        break;
        default:
        monthName = "Tidak ada bulan yang lebih dari 12";
        break;
     };
        
    console.log(newBioArr);
    console.log(monthName);
    console.log(dateArr);
    console.log(dateString);
    console.log(name);
}
      
      
      var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
      dataHandling2(input);

