//1. Membuat Kalimat
 var word = 'JavaScript';
 var second = 'is';
 var third = 'awesome';
 var fourth = 'and';
 var fifth = 'I';
 var sixth = 'love';
 var seventh = 'it!';
 console.log (word,second,third,fourth,fifth,sixth,seventh)



//2. Mengurai Kalimat (Akses karakter dalam string)
console.log('\n');
 var sentence = "I am going to be React Native Developer";
 var FirstWord = sentence [0];
 var SecondWord = sentence [2] + sentence [3];
 var thirdword = sentence [5] + sentence [6] + sentence [7] + sentence [8] + sentence [9];
 var fourthword = sentence [11] + sentence [12];
 var fifthword = sentence [14] + sentence [15];
 var sixthword = sentence [17] + sentence [18] + sentence [19] + sentence [20] + sentence [21];
 var seventhword = sentence [23] + sentence [24] + sentence [25] + sentence [26] + sentence [27] + sentence [28];
 var eighthword = sentence [30] + sentence [31] + sentence [32] + sentence [33] + sentence [34] + sentence [35] + sentence [36] + sentence [37] + sentence [38];
 console.log ('First Word : ' + FirstWord);
 console.log ('Second Word : ' + SecondWord);
 console.log ('Third Word : ' + thirdword);
 console.log ('Fourth Word : ' + fourthword);
 console.log ('Fifth Word : ' + fifthword);
 console.log ('Sixth Word : ' + sixthword);
 console.log ('Seventh Word : ' + seventhword);
 console.log ('Eighth Word : ' + eighthword);



//3. Mengurai Kalimat (Substring)
console.log('\n');
 var sentence2 = 'wow JavaScript is so cool';

 var firstword2 = sentence2.substring(0,3);
 var secondword2 = sentence2.substring(4,14);
 var thirdword2 = sentence2.substring(15,17);
 var fourthword2 = sentence2.substring(18,20);
 var fifthword2 = sentence2.substring(21,25);
 console.log('First Word : ' + firstword2);
 console.log('Second Word : ' + secondword2);
 console.log('Third Word : ' + thirdword2);
 console.log('Fourth Word : ' + fourthword2);
 console.log('Fifth Word : ' + fifthword2);




//4. Mengurai Kalimat dan Menentukan Panjang String
console.log('\n');
 var sentence3 = 'wow JavaScript is so cool';

 var firstword3 = sentence3.substring(0,3);
 var secondword3 = sentence3.substring(4,14);
 var thirdword3 = sentence3.substring(15,17);
 var fourthword3 = sentence3.substring(18,20);
 var fifthword3 = sentence3.substring(21,25);

 var firstwordlength = firstword3.length
 var secondwordlength = secondword3.length
 var thirdwordlength = thirdword3.length
 var fourthwordlength = fourthword3.length
 var fifthwordlength = fifthword3.length

 console.log('First Word : ' + firstword3 +' ,with length : ' + firstwordlength);
 console.log('Second Word : ' + secondword3 + ' ,with length : ' + secondwordlength);
 console.log('Third Word : ' + thirdword3 + ' ,with length : ' + thirdwordlength);
 console.log('Fourth Word : ' + fourthword3 + ' ,with length : ' + fourthwordlength);
 console.log('Fifth Word : ' + fifthword3 + ' ,with length : ' + fifthwordlength);
